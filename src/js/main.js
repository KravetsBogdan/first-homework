// 1) Оголосити зміни можна через let, const, var
// 2) prompt виводить модальне вікно із запитанням та полем для вводу та кнопками Ok та Cancel;
//    confirm виводить модальне вікно із запитанням і дві кнопки ok та cancel
// 3) Неявні перетворення типів це ті перетворення, які відбуваються в процесі роботи операторів
//    console.log('3' + 2)


// 1
let admin;
let myName = 'Bohdan';

admin = myName;
console.log(admin);

// 2
let days = 5;
console.log(days * 24 * 3600);

// 3
let age = prompt('Enter your age');

console.log(age);